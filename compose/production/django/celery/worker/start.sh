#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


celery -A django_docker.taskapp worker -l INFO
